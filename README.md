<h1>Casca NON-SGX </h1>

O aplicativo em desenvolvimento nesse repositório é uma parte do projeto Secure Cloud, e o aplicativo está sendo desenvolvido pela UTFPR-CT.

O objetivo da casca é fazer o intermedio de diversos serviços no futuro.

<b>Para contribuir com o projeto ler antes o arquivo CONTRIBUTING.md para saber os padrões aplicados nesse repositório. </b>

<br>
<br>
<br>

Pessoas que contribuem para esse projeto:<br>
<br>


Lucas Vale de Araújo<br>

Marcelo de Oliveira Rosa<br>

Pedro Emanuel Mateus Silva<br>
