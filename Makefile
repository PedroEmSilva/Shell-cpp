FLAGS := -lpthread -pthread 

MODE ?= 

FILES := global_data.cpp threads.cpp queue.cpp option.cpp socket.cpp commands.cpp API.cpp

OUTPUT_NAME :=  output

all:
	g++  $(MODE) $(FLAGS) $(FILES) -o $(OUTPUT_NAME)
clean:
	rm $(OUTPUT_NAME)
