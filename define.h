
#define MAX_CMD_PARAMS 10

//Numero de threads executando tarefas simultaneamente
//OBS: é um valor padrao, nao sera usado haja algum argumento passado pelo parametro -t
#define MAX_THREADS_DEFAULT 5

//Numero de tarefas que seram executadas(esse define eh temporario)
#define NUMBER_TASKS 100



//Numero maximo de sockets de IO permitidos
#define MAX_IO_SOCKETS 10
//Numero maximo de sockets de Server permitidos
#define MAX_SV_SOCKETS 10


//Timeout do listen (não funciona em infinito devido a necessidade de atualizar a lista)
#define SV_TIMEOUT_SECONDS 1 


//Timeout dos sockets de IO (não funciona em infinito devido a necessidade de atualizar a lista)
#define IO_TIMEOUT_SECONDS 1 

/*Tamanho do buffer de saída dos sockets de IO*/
#define SOCKET_BUFFER_SIZE 1024

/* */
#define MAX_READ_BYTES 256


