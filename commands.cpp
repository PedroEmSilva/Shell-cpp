	#include "commands.h"



void cmd_read (info_cmd_t* current_task)
{

	syslog(LOG_DEBUG, "[%lu]cmd_read() \n",pthread_self()%100);
	
	int n;
	char buffer[MAX_READ_BYTES];

	bzero(buffer,MAX_READ_BYTES);
	syslog(LOG_DEBUG, "[%lu]cmd_read: Lendo do socket %d \n",pthread_self()%100,current_task->socket->socket);
	n = read(current_task->socket->socket,buffer,MAX_READ_BYTES);
	syslog(LOG_DEBUG, "[%lu]cmd_read: Leitura do socket %d  realizada , buffer[0]= %d , n = %d\n",pthread_self()%100,current_task->socket->socket,(int)buffer[0],n);
	if (n < 0)
	{ 
		perror("ERROR reading from socket:\n");
		syslog(LOG_ERR, "[%lu]cmd_read: Erro de leitura, consultar stderr para mais detalhes\n",pthread_self()%100);

	}

	if((int)buffer[0] == 0&& n == 0)
	{
		syslog(LOG_ERR, "[%lu]cmd_read: Conexão finalizada pela outra ponta, finalizando socket\n",pthread_self()%100);
		current_task->socket->shutdown = 1;
	}
	syslog(LOG_DEBUG, "[%lu]cmd_read: Passando os dados recebidos para a função\n",pthread_self()%100);
	current_task->socket->socket_data_input.process(buffer,MAX_READ_BYTES);

	syslog(LOG_DEBUG, "[%lu]cmd_read: Função terminada \n",pthread_self()%100);
	
	current_task->socket->inRead = false;


}

void 
cmd_accept (info_cmd_t* current_task)
{

	syslog(LOG_DEBUG, "[%lu]cmd_accept() \n",pthread_self()%100);

	info_socket_t * master_info_socket = current_task->socket;

	struct sockaddr_in address = master_info_socket->info;
	int addrlen = sizeof(address);

	socket_t socket_to_accept = master_info_socket->socket;
	
	syslog(LOG_DEBUG, "[%lu]cmd_accept: Realizando Conexão \n",pthread_self()%100);
	int new_socket = accept(socket_to_accept, (struct sockaddr *)&address, (socklen_t*)&addrlen);
	printf("fd ew sock = %d\n",new_socket );
	if (new_socket < 0)
	{
	     		
		syslog(LOG_ERR, "[%lu]cmd_accept: Não foi possivel conectar, removendo socket \n",pthread_self()%100);
		shutdown(new_socket, SHUT_RDWR);
					  
		return ;      
	}
	syslog(LOG_DEBUG, "[%lu]cmd_accept: Nova Conexão fd = %d , ip é : %s , porta : %d \n" ,pthread_self()%100,new_socket , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));

	


	
	accept_socket_data* data = (accept_socket_data*)current_task->data_slot;
	pthread_mutex_t *IOvec_mutex = data->IOvec_mutex;
	info_socket_t*  IOvec = data->IOvec; 
	


syslog(LOG_DEBUG, "[%lu]cmd_accept: Aguardando accesso ao vetor de sockets de IO \n",pthread_self()%100);
pthread_mutex_lock(IOvec_mutex);

syslog(LOG_DEBUG, "[%lu]cmd_accept: Accesso ao vetor de sockets de IO concedido \n",pthread_self()%100);

//Arrumar para caso não haja slots
	for (int i = 0; i < MAX_IO_SOCKETS; ++i)
	{
		if(IOvec[i].socket==0)
		{
			syslog(LOG_DEBUG, "[%lu]cmd_accept: Slot de socket encontrado do vetor de sockets de IO \n",pthread_self()%100);
			IOvec[i].socket= new_socket;
			IOvec[i].socket_data_input.process = master_info_socket->socket_data_input.process;
			IOvec[i].inRead = false;
			IOvec[i].inWrite = false;
			IOvec[i].select_ready = true;
			IOvec[i].socket_data_output.buffer = malloc(SOCKET_BUFFER_SIZE*sizeof(char));
			IOvec[i].socket_data_output.free_index = 0; 
			IOvec[i].shutdown = 0; 
			IOvec[i].isConnected = true;
			pthread_mutex_unlock(IOvec_mutex);
	
			current_task->socket->select_ready =true;

			syslog(LOG_DEBUG, "[%lu]cmd_accept: Comando executado com sucesso \n",pthread_self()%100);

			return ;
		}

	}

	pthread_mutex_unlock(IOvec_mutex);

	syslog(LOG_DEBUG, "[%lu]cmd_accept: Não foi possivel encontrar um slot no vetor de IO \n",pthread_self()%100);

	shutdown(new_socket, SHUT_RDWR);
	
	return ;

}

void cmd_write(info_cmd_t* current_task) 
{
	//current_task->socket->inWrite = true;
	syslog(LOG_DEBUG, "[%lu]cmd_write()\n",pthread_self()%100);
	
	syslog(LOG_DEBUG, "[%lu]cmd_write: Escrevendo '%s' no socket %d  tam = %lu\n",pthread_self()%100,(char *)current_task->socket->socket_data_output.buffer,current_task->socket->socket,SOCKET_BUFFER_SIZE -(SOCKET_BUFFER_SIZE- current_task->socket->socket_data_output.free_index));
	long sent =write(current_task->socket->socket ,(char *)current_task->socket->socket_data_output.buffer , SOCKET_BUFFER_SIZE -(SOCKET_BUFFER_SIZE- current_task->socket->socket_data_output.free_index)) ;
	//long sent =write(current_task->socket->socket ,(char *)current_task->socket->socket_data_output.buffer ,3 ) ;
	
		 if( sent <= 0)
        {

            return ;
        }

        syslog(LOG_DEBUG, "[%lu]cmd_write: Foram enviados %lu bytes\n",pthread_self()%100, sent);
        char * aux = (char *)current_task->socket->socket_data_output.buffer;

        memcpy(current_task->socket->socket_data_output.buffer,&aux[sent],SOCKET_BUFFER_SIZE-sent );
        current_task->socket->socket_data_output.free_index -= sent;
    	
    	if(current_task->socket->socket_data_output.free_index == 0 )
    	{
    		current_task->socket->socket_data_output.send_done();
    	}
        
        current_task->socket->inWrite = false;
       

}

//conecta com o endereço e port definidos na info do info socket
void cmd_connect(info_cmd_t* current_task){

	syslog(LOG_DEBUG, "[%lu]cmd_connect()\n",pthread_self()%100);

	struct sockaddr_in server = current_task->socket->info;

	syslog(LOG_DEBUG, "[%lu]cmd_connect: Tentando conexão\n",pthread_self()%100);
	
	if (connect(current_task->socket->socket , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
       syslog(LOG_ERR, "[%lu]cmd_accept: Não foi possivel conectar, removendo socket \n",pthread_self()%100);
		current_task->socket->shutdown = 1;
		current_task->socket->isConnected = false;
        return ;
    }	

    current_task->socket->isConnected = true;
    current_task->connected(NULL);

    syslog(LOG_DEBUG, "[%lu]cmd_connect: Conexão realizada com sucesso\n",pthread_self()%100);
   
}