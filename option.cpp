#include "option.h"

void
print_usage()
{
	printf("Usage: ./output -t<Numero de threads> \n");
	exit(1);
}


/*
Função que analiza as opções/flags passadas pelo usuário na execução do programa
*/
void
use_getopt(int * argc, char *** argv, config_struct_t * config)
{
	int options;
	//Flag q indica se o usuario digitou -t na execucao
	int tflag = 0;

	while( ( options = getopt(*argc, *argv, "t::") ) != -1)
	{
		switch(options)
		{
			case 't':
			{
				if(optarg == NULL)
					print_usage();
				tflag = 1;
				//obtem o argumento de -t
				errno = 0;
				char * aux;

				long t_arg_long = strtol(optarg, &aux, 10); 
				

				//Detectando falhas na conversao(usuario pode ter passado argumentos invalidos com -t)
				//Melhorar essa detecao de falhas
				if (aux == optarg)
    				print_usage();
				if ((t_arg_long == LONG_MAX || t_arg_long == LONG_MIN) && errno == ERANGE)  
    				print_usage();
				if ( (t_arg_long > INT_MAX) || (t_arg_long < INT_MIN) )
    				print_usage();	

    			int t_arg = (int) (t_arg_long);
				//Numero de threads executando tarefas simultaneamente
				config->max_threads = t_arg;
			
				break;
			}
			case '?':
			{
				if(optopt == 't')
				{
					exit(1);
				}
				else
				{
					printf("Unknow input	 %c.\n", optopt);
					print_usage();
				}
				break;
			}
			default:
			{
				printf("Entrada invalida.\n");
				print_usage();
				break;
			}
		}
	}

}