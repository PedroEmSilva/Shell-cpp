#include "threads.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "commands.h"


int taskID =0;
bool running = 1;



/*
	Função que as threads de execução vão ficar executando.
*/    		
void stop_all()
{
	running = 0;
}




void* do_task(void * arg)
{
	int num_tasks;

	syslog(LOG_DEBUG, "[%lu]do_task()\n",pthread_self()%100);

	do_task_args_t * parameters = (do_task_args_t*) arg;

	pthread_mutex_t * mutex_tasks = parameters->mutex_tasks;

	pthread_mutex_lock(mutex_tasks);

	info_cmd_queue_t ** tasks = parameters->tasks;
	sem_t * semaphore = parameters->semaphore;  

	pthread_mutex_unlock(mutex_tasks);

	info_socket_t * vec = parameters->vec;

	int  vec_lenght = parameters->vec_lenght;

	info_cmd_t * current_task = NULL;

	syslog(LOG_DEBUG, "[%lu]do_task: Parametros recebidos\n",pthread_self()%100);

	/*Loop de busca e execução de comandos*/
	do
	{
		syslog(LOG_DEBUG, "[%lu]do_task: Aguardando task\n",pthread_self()%100);
		
		sem_wait(semaphore);

		syslog(LOG_DEBUG, "[%lu]do_task: Task detectada na fila\n",pthread_self()%100);

		get_task(tasks, &current_task, mutex_tasks);

		syslog(LOG_DEBUG, "[%lu]do_task: Task recebida\n",pthread_self()%100);

		if(current_task != NULL)
		{
			syslog(LOG_DEBUG, "[%lu]do_task: Task não nula, executando comando\n",pthread_self()%100);

			current_task->cmd(current_task);

			syslog(LOG_DEBUG, "[%lu]do_task: Task concluida\n",pthread_self()%100);

			if(current_task->data_slot!= NULL)
			{
				//free (current_task->data_slot);
			}
			
			if(current_task!= NULL)
			{
				//Erro: free crashes
				//free (current_task);
			}
		}
		sem_getvalue(semaphore,  &num_tasks);
	}while(running == 1 );
}

/*
Função que pega o primeiro elemento da fila de tarefas e carrega em uma variavel
O parametro tasks é a fila de tarefas
O parametro current_task é o endereço onde será carregado a tarefa removida da fila
O parametro é um ponteiro para o mutex da fila de tarefas
*/
void
get_task(info_cmd_queue_t ** tasks, info_cmd_t ** current_task, pthread_mutex_t * mutex_tasks)
{	
	syslog(LOG_DEBUG, "[%lu]get_task()\n",pthread_self()%100);

	syslog(LOG_DEBUG, "[%lu]get_task: Aguardando task\n",pthread_self()%100);

	pthread_mutex_lock(mutex_tasks);

	syslog(LOG_DEBUG, "[%lu]get_task: Task detectada na fila\n",pthread_self()%100);

	if(*tasks != NULL)
	{
		info_cmd_queue_t * aux = (info_cmd_queue_t *) queue_remove( (queue_t **)tasks, (queue_t *)*tasks);
		*current_task = &(aux->cmd);

		syslog(LOG_DEBUG, "[%lu]get_task: Task retirada da fila\n",pthread_self()%100);
	} 
	else
	{
		* current_task = NULL; 
		syslog(LOG_DEBUG, "[%lu]get_task: Erro, task nula\n",pthread_self()%100);
	}

	pthread_mutex_unlock(mutex_tasks);

	syslog(LOG_DEBUG, "[%lu]get_task: task retirada da fila com sucesso\n",pthread_self()%100);
}


/*
Função que adiciona uma nova task para fila de tarefas
O parametro task_queue é a fila de tarefas que sera adicionada a tarefa
O parametro task é a tarfa a ser adicionada
O parametro semaphore é o semafaro de acesso a fila
*/
int
add_task(info_cmd_queue_t ** task_queue, info_cmd_queue_t * task, sem_t * semaphore, pthread_mutex_t * tasks_mutex)
{
	syslog(LOG_DEBUG, "[%lu]add_task()\n",pthread_self()%100);

	if(task == NULL)
	{
		syslog(LOG_ERR, "[%lu]add_task: Task nula, abortando operação\n",pthread_self()%100);
		return -1;
	}
	if(semaphore == NULL)
	{
		syslog(LOG_ERR, "[%lu]add_task: Semaforo nulo, abortando operação\n",pthread_self()%100);
		return -1;
	}
	if(tasks_mutex == NULL)
	{
		syslog(LOG_ERR, "[%lu]add_task: Mutex nulo, abortando operação\n",pthread_self()%100);
		return -1;
	}

	pthread_mutex_lock(tasks_mutex);
	
	int err = queue_append ((queue_t **) task_queue, (queue_t*) task);
	
	if(err<0)
	{
		syslog(LOG_ERR, "[%lu]add_task: Erro na inserção na fila, abortando operação\n",pthread_self()%100);
		return -1;
	}
	
	sem_post(semaphore);
	
	pthread_mutex_unlock(tasks_mutex);
	
	syslog(LOG_DEBUG, "[%lu]add_task: Task adicionada com sucesso\n",pthread_self()%100);

	return 0;
}


void *
listen_select_task (void * arg)
{
	syslog(LOG_DEBUG, "[%lu]listen_select_task()\n",pthread_self()%100);

	select_task_args_t * parameters = (select_task_args_t*) arg;

	info_socket_t * vec = parameters->vec;

	int  vec_lenght = parameters->vec_lenght;

	info_cmd_queue_t ** tasks = parameters->tasks;

	pthread_mutex_t * vec_mutex = parameters->vec_mutex;
	pthread_mutex_t * tasks_mutex = parameters->tasks_mutex;
	pthread_mutex_t * IOvec_mutex = parameters->IOvec_mutex;

	info_socket_t * IOvec = parameters->IOvec;

	sem_t * tasks_sem = parameters->tasks_sem;

	fd_set listenSet;

	syslog(LOG_DEBUG, "[%lu]listen_select_task: Inicialização completa\n",pthread_self()%100);

	while(running)
	{
		FD_ZERO(&listenSet);
		
		long int maxFd=-1;

		int serverActivity;
		
		pthread_mutex_lock(vec_mutex);
		
		for (int i = 0; i < vec_lenght; ++i)
		{			
			if(vec[i].shutdown == 1 && vec[i].socket!=0)
			{
				syslog(LOG_DEBUG, "[%lu]listen_select_task: Shutting down socket %d\n",pthread_self()%100,vec[i].socket);
				int error = shutdown(vec[i].socket,SHUT_RDWR);
				if(error < 0)
				{
					perror("Erro shutdown() em server select:");
				}

				vec[i].socket = 0;
			}

			if(vec[i].select_ready)
			{
				if(vec[i].socket > maxFd)
				{
					maxFd = vec[i].socket;
				}
			
				FD_SET(vec[i].socket, &listenSet);
			}
			
		}
		pthread_mutex_unlock(vec_mutex);

		struct timeval a ;

		a.tv_sec = SV_TIMEOUT_SECONDS;

		serverActivity = select( maxFd + 1 , &listenSet , NULL , NULL , &a);	
		
		if(serverActivity == 0)
		{
			syslog(LOG_DEBUG, "[%lu]listen_select_task: Nenhuma atividade aconteceu\n",pthread_self()%100);
		}

		if ((serverActivity < 0) && (errno != EINTR)) 
		{
			syslog(LOG_DEBUG, "[%lu]listen_select_task: Ocorreu algum erro no select da threads\n",pthread_self()%100);
		}

		for (int i = 0; i < vec_lenght; ++i)
		{
	
			if(vec[i].type == SERVER && FD_ISSET(vec[i].socket, &listenSet))
			{	
				syslog(LOG_DEBUG, "[%lu]listen_select_task: Atividade no socket %d\n",pthread_self()%100, vec[i].socket);

				vec[i].select_ready = false;

				info_cmd_queue_t* new_task = (info_cmd_queue_t*)malloc(sizeof(info_cmd_queue_t));
 
				new_task->cmd.cmd = (void (*)(void *))&cmd_accept;
				new_task->cmd.socket = &vec[i];
				new_task->next =NULL;
				new_task->prev =NULL;
				new_task->id = taskID;
				new_task->cmd.data_slot = malloc(sizeof(accept_socket_data));

				accept_socket_data acceptCmd_args;
				acceptCmd_args.IOvec = IOvec;
				acceptCmd_args.IOvec_mutex = IOvec_mutex;

				new_task->cmd.data_slot = (void *) &acceptCmd_args;

				add_task(tasks, new_task, tasks_sem, tasks_mutex);

				taskID++;
			}

		}


		
	}
}


void *
IO_select_task (void * arg)
{
	syslog(LOG_DEBUG, "[%lu]IO_select_task()\n",pthread_self()%100);

	select_task_args_t * parameters = (select_task_args_t*) arg;

	info_socket_t * vec = parameters->IOvec;

	int  vec_lenght = parameters->vec_lenght;

	pthread_mutex_t * vec_mutex = parameters->IOvec_mutex;

	info_cmd_queue_t ** tasks = parameters->tasks;

	pthread_mutex_t * tasks_mutex = parameters->tasks_mutex;

	sem_t * tasks_sem = parameters->tasks_sem;

	fd_set readSet;

	fd_set writeSet;

	syslog(LOG_DEBUG, "[%lu]IO_select_task: Inicialização completa\n",pthread_self()%100);

	while(running)
	{
		FD_ZERO(&readSet);

		FD_ZERO(&writeSet);

		long int maxFd=-1;

		int IOActivity;

		pthread_mutex_lock(vec_mutex);

		for (int i = 0; i < vec_lenght; ++i)
		{	
			if(vec[i].shutdown == 1 && vec[i].socket != 0 && vec[i].socket_data_output.free_index == 0)
			{
				syslog(LOG_DEBUG, "[%lu]IO_select_task: Shutting down socket %d   Connected = %d\n",pthread_self()%100,vec[i].socket,vec[i].isConnected);
				if(vec[i].isConnected)
				{	
					int error = shutdown(vec[i].socket,SHUT_RDWR);
				
					if(error < 0)
					{
						/*erro ao desligar o socket*/
						perror("Erro shutdown() em IO select:");
					}
				}
				vec[i].socket = 0;
			}


			if(vec[i].socket!= 0)
			{
				if(vec[i].socket > maxFd)
				{
					maxFd = vec[i].socket;
					
				}

				if(vec[i].select_ready==true)
				{	
					if(vec[i].shutdown == 0) /*Em caso de desligamento, nenhuma leitura pode ser feita, mas as escritas continuam até o buffer esvaziar*/
					{
						FD_SET(vec[i].socket, &readSet);	
					}
						
					if(vec[i].socket_data_output.free_index >= 1 && vec[i].inWrite == false)/*Um socket só necessita saber se pode ser escrito quando tem-se algo para escrever nele que não esta sendo escrito por outra thread*/
					{
						FD_SET(vec[i].socket, &writeSet);
					}
				}
			}
			
		}
		pthread_mutex_unlock(vec_mutex);

		struct timeval b;

		b.tv_sec = IO_TIMEOUT_SECONDS;

		IOActivity = select( maxFd + 1 , &readSet , &writeSet , NULL , &b);	

		if(IOActivity == 0)
		{
			syslog(LOG_DEBUG, "[%lu]IO_select_task: Nenhuma atividade aconteceu\n",pthread_self()%100);
		}

		if ( (IOActivity < 0) && (errno!=EINTR)) 
		{
			printf("Ocorreu algum erro no select da threads %u", (unsigned int)pthread_self());
		}

		for (int i = 0; i < vec_lenght; ++i)
		{
			if(FD_ISSET(vec[i].socket, &readSet) && FD_ISSET(vec[i].socket, &writeSet))
			{
				if(vec[i].inRead == false)
				{
					info_cmd_queue_t* new_task = (info_cmd_queue_t*)malloc(sizeof(info_cmd_queue_t));
					
					new_task->cmd.cmd = (void (*)(void *))&cmd_read;
					new_task->cmd.socket = &vec[i];
					new_task->next =NULL;
					new_task->prev =NULL;
					new_task->id = taskID;
					new_task->cmd.data_slot = malloc(sizeof(accept_socket_data));

					accept_socket_data acceptCmd_args;

					new_task->cmd.data_slot = (void *) &acceptCmd_args;

					add_task(tasks, new_task, tasks_sem, tasks_mutex);

					vec[i].inRead = true;

					taskID++;
				}

				if(vec[i].inWrite == false && vec[i].socket_data_output.buffer != NULL )
				{
					info_cmd_queue_t* new_task = (info_cmd_queue_t*)malloc(sizeof(info_cmd_queue_t));

					new_task->cmd.cmd =(void (*)(void *))&cmd_write;
					new_task->cmd.socket = &vec[i];
					new_task->next =NULL;
					new_task->prev =NULL;
					new_task->id = taskID;

					add_task(tasks, new_task, tasks_sem, tasks_mutex);

					vec[i].inWrite = true;
				}
			}
			else if(FD_ISSET(vec[i].socket, &readSet) && !FD_ISSET(vec[i].socket, &writeSet))
			{
				if(vec[i].inRead == false)
				{
					info_cmd_queue_t* new_task = (info_cmd_queue_t*)malloc(sizeof(info_cmd_queue_t));

					new_task->cmd.cmd = (void (*)(void*))&cmd_read;
					new_task->cmd.socket = &vec[i];
					new_task->next =NULL;
					new_task->prev =NULL;
					new_task->id = taskID;

					taskID++;

					new_task->cmd.data_slot = (info_cmd_queue_t*)malloc(sizeof(accept_socket_data));

					accept_socket_data acceptCmd_args;

					new_task->cmd.data_slot = (void *) &acceptCmd_args;

					add_task(tasks, new_task, tasks_sem, tasks_mutex);
    		
					vec[i].inRead = true;
				}
			}
			else if(!FD_ISSET(vec[i].socket, &readSet) && FD_ISSET(vec[i].socket, &writeSet))
			{
				if(vec[i].inWrite == false && vec[i].socket_data_output.buffer != NULL )
				{
					syslog(LOG_DEBUG, "[%lu]IO_select_task: Atividade no socket %d escrita\n",pthread_self()%100, vec[i].socket);

					info_cmd_queue_t* new_task = (info_cmd_queue_t*)malloc(sizeof(info_cmd_queue_t));

					new_task->cmd.cmd =(void (*)(void *))&cmd_write;
					new_task->cmd.socket = &vec[i];
					new_task->next =NULL;
					new_task->prev =NULL;
					new_task->id = taskID;

					vec[i].inWrite = true;

					add_task(tasks, new_task, tasks_sem, tasks_mutex);
				}
			}
		}
	}
}
