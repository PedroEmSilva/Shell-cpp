#include <pthread.h>
#include <semaphore.h>
//#include "socket_data.h"
#include "data_structs.h"
#include "threads.h"
#include <stdio.h>

/* vetor de sockets para processamento */
info_socket_t  info_sockets[INFO_SOCKETS_SIZE];
pthread_mutex_t  info_sockets_mutex;

/* fila FIFO (duplamente encadeada) de mensagens para pool de thread
   contém o topo da lista.
   tp_* de thread_pool_* */
info_cmd_queue_t* 	tp_msgs = NULL;
pthread_mutex_t     tp_mutex = PTHREAD_MUTEX_INITIALIZER;
sem_t 			 	tp_sem;




void print_elem (void *ptr)
{
   info_cmd_queue_t *elem = ptr ;

   if (!elem)
      return ;

   elem->prev ? printf ("%d", elem->prev->id) : printf ("*") ;
   printf ("<%d>", elem->id) ;
   elem->next ? printf ("%d ", elem->next->id) : printf ("*") ;
}

int main()
{
	pthread_t thread[MAX_THREADS];

	int id_threads[MAX_THREADS];

	int i;

	do_task_args_t aux;

	//Colocando varios elementos na fila de tarefas
	// inicializa os N elementos
	info_cmd_queue_t item[NUMBER_TASKS];
   	for ( i = 0; i<NUMBER_TASKS; i++)
   	{
    	item[i].id = i ;
    	item[i].prev = NULL ;
    	item[i].next = NULL ;
   	}
   	//Adicionando os elemento iniciados na fila de tarefas à fazer.
   	for (i=0; i<NUMBER_TASKS; i++)
   	{
   		add_task(&tp_msgs, &item[i], &tp_sem);

   	}

	queue_print ("Fila apos inserção  ", (queue_t*) tp_msgs, print_elem) ;

	//Iniciando todas as tasks
	for(i = 0; i < MAX_THREADS; i++ )
	{		
		aux.tasks = &tp_msgs;
		aux.mutex_tasks = &tp_mutex;
		aux.semaphore = &tp_sem;
		id_threads[i] = pthread_create( &thread[i], NULL, &do_task, (void*) &aux );
	}


	//Join nas threads
	for(i = 0; i < MAX_THREADS; i++ )
	{
		pthread_join( thread[i], NULL);
	}

	return 0;
}