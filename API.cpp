	#include "API.h"

	//Sockets de IO
info_socket_t * IO_isocket_vec;
pthread_mutex_t * IO_isocket_vec_mutex ;
	//Sockets de Server
info_socket_t * SV_isocket_vec;
pthread_mutex_t * SV_isocket_vec_mutex;

	/* fila FIFO (duplamente encadeada) de mensagens para pool de thread
		contém o topo da lista.
		tp_* de task_pool_* */
info_cmd_queue_t* 	tp_msgs = NULL;
pthread_mutex_t     tp_mutex = PTHREAD_MUTEX_INITIALIZER;
sem_t 			 	tp_sem ;

	/*Função para ser utilizada ao receber um pacote de bytes caso o usuário não especifique uma*/
long process_default(void* msg, long tam)
{
	syslog(LOG_WARNING, "process_default: Essa função é um placeholder");

	printf("Process default message!(%lu): %s\n",tam,(char*)msg );

	return 0;
}	
	/*Função para ser utilizada ao terminar de enviar bytes caso o usuario não especifique uma*/
void send_done_default()
{
	syslog(LOG_WARNING, "send_done_default: Essa função é um placeholder");

	printf("Send done default message!\n");

	return;
}	

void connect_sucessful_default()
{
	syslog(LOG_WARNING, "connect_sucessful_default: Essa função é um placeholder");

	printf("Connected default message!\n");

	return;
}

	/*Função para inicializar a casca com base nos parametros definidos no define.h e nos passados por argv[]*/
void API_Init(int argc, char * argv[])
{	
	syslog(LOG_INFO, "API: Inicializando casca ...\n");
	syslog(LOG_DEBUG, "[%lu]API_Init()",(long unsigned int)pthread_self()%100);

	sem_init(&tp_sem, 1, 0);
		
	IO_isocket_vec_mutex = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
	pthread_mutex_unlock(IO_isocket_vec_mutex);

	IO_isocket_vec = (info_socket_t*)malloc(MAX_IO_SOCKETS*sizeof(info_socket_t));

	SV_isocket_vec_mutex = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
	pthread_mutex_unlock(SV_isocket_vec_mutex);

	SV_isocket_vec = (info_socket_t*)malloc(MAX_SV_SOCKETS*sizeof(info_socket_t));

	for (int i = 0; i < MAX_IO_SOCKETS; ++i)
	{
		IO_isocket_vec[i].socket=0;
	}
	for (int i = 0; i <MAX_SV_SOCKETS; ++i)
	{
		SV_isocket_vec[i].socket=0;
	}

	config_struct_t config;
	config.max_threads = MAX_THREADS_DEFAULT;
	use_getopt(&argc,  &argv, &config);

	pthread_t thread[config.max_threads];

	syslog(LOG_DEBUG, "[%lu]API_Init: Memória alocada",(long unsigned int)pthread_self()%100);

	process_func_t p_func = &process_default;

	select_task_args_t select_args;

	select_args.vec = SV_isocket_vec;
	select_args.vec_lenght = MAX_SV_SOCKETS;
	select_args.timeout =  SV_TIMEOUT_SECONDS;
	select_args.vec_mutex = SV_isocket_vec_mutex;
	select_args.tasks = &tp_msgs;
	select_args.tasks_mutex = &tp_mutex;
	select_args.tasks_sem = &tp_sem;
	select_args.IOvec =IO_isocket_vec;
	select_args.IOvec_mutex = IO_isocket_vec_mutex;

	syslog(LOG_DEBUG, "[%lu]API_Init: Iniciando a thread de server listen",(long unsigned int)pthread_self()%100);

	pthread_create( &thread[0], NULL, &listen_select_task, (void*) &select_args );

	syslog(LOG_DEBUG, "[%lu]API_Init: Thread de server listen lançada",(long unsigned int)pthread_self()%100);

	select_task_args_t IO_select_args;
	IO_select_args.vec = IO_isocket_vec;
	IO_select_args.vec_lenght = MAX_IO_SOCKETS;
	IO_select_args.timeout = IO_TIMEOUT_SECONDS;
	IO_select_args.vec_mutex = SV_isocket_vec_mutex;
	IO_select_args.tasks = &tp_msgs;
	IO_select_args.tasks_mutex = &tp_mutex;
	IO_select_args.tasks_sem = &tp_sem;
	IO_select_args.IOvec =IO_isocket_vec;
	IO_select_args.IOvec_mutex = IO_isocket_vec_mutex;

	syslog(LOG_DEBUG, "[%lu]API_Init: Iniciando a thread de IO listen",(long unsigned int)pthread_self()%100);

	pthread_create( &thread[1], NULL, &IO_select_task, (void*) &IO_select_args );

	syslog(LOG_DEBUG, "[%lu]API_Init: Thread de IO listen lançada",(long unsigned int)pthread_self()%100);

	do_task_args_t task_args;

	for(int i = 2; i < 10; i++ )
	{		
		syslog(LOG_DEBUG, "[%lu]API_Init:Iniciando a thread %d (worker)\n",(long unsigned int)pthread_self()%100, i);

		task_args.tasks = &tp_msgs;
		task_args.mutex_tasks = &tp_mutex;
		task_args.semaphore = &tp_sem;

		pthread_create( &thread[i], NULL, &do_task, (void*) &task_args );
	}

	sleep(1);

	syslog(LOG_INFO, "API: Casca inicialzada\n");
}




	/*Função para criar um socket que escuta a porta passada por referencia e cria sockets de IO que chamam as funções passadas ao receber/enviar dados*/
info_socket_t* API_create_server(int porta, send_done_func_t* send_done_func, process_func_t* process_func)
{
	syslog(LOG_INFO, "API: Criando Socket de server\n");
	
	syslog(LOG_DEBUG, "[%lu]API_create_server() ",(long unsigned int)pthread_self()%100);

	if(porta <= 0)
	{
		syslog(LOG_ERR, "[%lu]API_create_server: Porta invalida",(long unsigned int)pthread_self()%100);
		return NULL;
	}

	syslog(LOG_DEBUG, "[%lu]API_create_server: Esperando acesso ao vetor de sockets de server",(long unsigned int)pthread_self()%100);

	pthread_mutex_lock(SV_isocket_vec_mutex);
	
	syslog(LOG_DEBUG, "[%lu]API_create_server: Acesso ao vetor de sockets de server concedido",(long unsigned int)pthread_self()%100);
	
	for (int i = 0; i < MAX_SV_SOCKETS; ++i)
	{

		if(SV_isocket_vec[i].socket==0)
		{

			if(process_func == NULL)
			{
				syslog(LOG_WARNING, "[%lu]API_create_server: Função 'process_func' nula, atribuindo função padrão",(long unsigned int)pthread_self()%100);
				SV_isocket_vec[i].socket_data_input.process = &process_default;
			}
			else
			{
				SV_isocket_vec[i].socket_data_input.process = *process_func;
			}

			if(send_done_func == NULL)
			{
				syslog(LOG_WARNING, "[%lu]API_create_server: Função 'send_done_func' nula, atribuindo função padrão",(long unsigned int)pthread_self()%100);
				SV_isocket_vec[i].socket_data_output.send_done = &send_done_default;
			}
			else
			{
				SV_isocket_vec[i].socket_data_output.send_done = *send_done_func;
			}

			syslog(LOG_DEBUG, "[%lu]API_create_server: Inicializando socket de listen",(long unsigned int)pthread_self()%100);

			SV_isocket_vec[i].type = SERVER;

			int error = socket_init(&SV_isocket_vec[i],porta);
			
			if(error < 0)
			{
				syslog(LOG_ERR, "[%lu]API_create_server: Erro na inicialização do socket, abortando função",(long unsigned int)pthread_self()%100);
				return NULL;
			}

			syslog(LOG_DEBUG, "[%lu]API_create_server: Socket inicializado ",(long unsigned int)pthread_self()%100);	

			pthread_mutex_unlock(SV_isocket_vec_mutex);

			syslog(LOG_DEBUG, "[%lu]API_create_server: Novo socket de server inserido na posição %d fd = %d",(long unsigned int)pthread_self()%100,i,SV_isocket_vec[i].socket);

			syslog(LOG_INFO, "API: Socket de server criado com sucesso\n");

			return &SV_isocket_vec[i];
		}

	}

	pthread_mutex_unlock(SV_isocket_vec_mutex);
	
	syslog(LOG_ERR, "[%lu]API_create_server: Não foi encontrado um slot disponivel no vetor de servers\n",(long unsigned int)pthread_self()%100);

	return NULL;
}





	/*API_create_socket */
	/*
	  Cria um socket e o conecta ao endereço desejado na porta referenciada caso exista espaço no vetor, 
	  Quando o socket receber algum dado será chamada a função "process_func" do tipo: long (* process_func_t)(void * buffer, long buffer_used)
	  Quando o socket terminar de mandar os dados será chamada a função "send_done_func" do tipo: void (* send_done_func_t)();
	  Caso as funções sejam nulas, são atribuidas funções padrão de processamento
	  Caso não exista espaço retorna NULL
	  Caso não consiga conectar ...
	  Caso consiga criar o socket corretamente retorna um ponteiro para o mesmo
	*/
info_socket_t* API_create_socket(char*  address,int porta,send_done_func_t* send_done_func, process_func_t* process_func,task_func* connected)
{
	syslog(LOG_INFO, "API: Criando Socket de IO\n");

	syslog(LOG_DEBUG, "[%lu]API_create_socket()",(long unsigned int)pthread_self()%100);

	if(address == NULL)
	{
		syslog(LOG_ERR, "[%lu]API_create_socket: Endereço nulo",(long unsigned int)pthread_self()%100);
		return NULL;
	}
	if(porta <= 0)
	{
		syslog(LOG_ERR, "[%lu]API_create_socket: Porta invalida",(long unsigned int)pthread_self()%100);
		return NULL;
	}

	pthread_mutex_lock(IO_isocket_vec_mutex );

	for (int i = 0; i < MAX_IO_SOCKETS; ++i)
	{

		if(IO_isocket_vec[i].socket==0)
		{

			syslog(LOG_DEBUG, "[%lu]API_create_socket: Slot de socket de IO encontrado",(long unsigned int)pthread_self()%100);
			if(process_func == NULL)
			{
				syslog(LOG_WARNING, "[%lu]API_create_socket: Função 'process_func' nula, atribuindo função padrão",(long unsigned int)pthread_self()%100);
				IO_isocket_vec[i].socket_data_input.process = &process_default;
			}
			else
			{
				IO_isocket_vec[i].socket_data_input.process = *process_func;
			}

			if(send_done_func == NULL)
			{
				syslog(LOG_WARNING, "[%lu]API_create_socket: Função 'send_done_func' nula, atribuindo função padrão",(long unsigned int)pthread_self()%100);
				IO_isocket_vec[i].socket_data_output.send_done = &send_done_default;
			}
			else
			{
				IO_isocket_vec[i].socket_data_output.send_done = *send_done_func;
			}

			syslog(LOG_DEBUG, "[%lu]API_create_socket: Funções de processamento definidas",(long unsigned int)pthread_self()%100);

			IO_isocket_vec[i].type = IO; 

			int err = socket_init(&IO_isocket_vec[i] , 0);

			if(err < 0)
			{
				syslog(LOG_ERR, "[%lu]API_create_socket: Falha na inicialização do socket, abortando função",(long unsigned int)pthread_self()%100);
				return NULL;
			}

			syslog(LOG_DEBUG, "[%lu]API_create_socket: Socket inicializado",(long unsigned int)pthread_self()%100);


			IO_isocket_vec[i].inRead = false;
			IO_isocket_vec[i].inWrite = false;
			IO_isocket_vec[i].info.sin_addr.s_addr = inet_addr(address);
			IO_isocket_vec[i].info.sin_family = AF_INET;
			IO_isocket_vec[i].info.sin_port = htons( porta);
			IO_isocket_vec[i].select_ready = false;
			IO_isocket_vec[i].socket_data_output.buffer = malloc(SOCKET_BUFFER_SIZE*sizeof(char));
			IO_isocket_vec[i].socket_data_output.free_index = 0;
			IO_isocket_vec[i].select_ready = true;

			info_cmd_queue_t* new_task = (info_cmd_queue_t*)malloc(sizeof(info_cmd_queue_t));

			new_task->cmd.cmd = (void (*)(void*)) &cmd_connect;
			new_task->cmd.socket = &IO_isocket_vec[i];
			new_task->next =NULL;
			new_task->prev =NULL;

			if(connected == NULL)
			{
				new_task->cmd.connected = (void (*)(void*))&connect_sucessful_default;
			}
			else
			{
				new_task->cmd.connected = (void (*)(void*))*connected;
			}

			

			pthread_mutex_unlock(IO_isocket_vec_mutex );

			syslog(LOG_DEBUG, "[%lu]API_create_socket: Socket criado com sucesso",(long unsigned int)pthread_self()%100);

			syslog(LOG_DEBUG, "[%lu]API_create_socket: Adicionando task de conexão",(long unsigned int)pthread_self()%100);

			int error = add_task(&tp_msgs, new_task, &tp_sem, &tp_mutex);

			if(error < 0 )
			{
				syslog(LOG_ERR, "[%lu]API_create_socket: Não foi possivel inserir a task de conexão na fila, abortando função",(long unsigned int)pthread_self()%100);
				return NULL;
			}

			syslog(LOG_INFO, "API: Socket de IO criado com sucesso\n");

			return &IO_isocket_vec[i];
		}

	}

	pthread_mutex_unlock(IO_isocket_vec_mutex );

	syslog(LOG_ERR, "[%lu]API_create_socket: Não foi encontrado um slot no vetor de IO",(long unsigned int)pthread_self()%100);

	return NULL;
}


int API_write(info_socket_t* socket,void* msg,long int msg_size)
{
	if(socket->shutdown == 1)
	{
		syslog(LOG_ERR, "[%lu]API_write: Socktet em processo de desligamento, a mensagem não será enviada",(long unsigned int)pthread_self()%100);
		return -1;
	}
	
	if(socket->isConnected == 0)
	{
		syslog(LOG_ERR, "[%lu]API_write: Desconctado, não foi possivel escrever a mensagem",(long unsigned int)pthread_self()%100);
		return -1;
	}


	syslog(LOG_INFO, "API: Escrevendo mensagem no socket\n");

	syslog(LOG_DEBUG, "[%lu]API_write()",(long unsigned int)pthread_self()%100);
	
	if(socket == NULL)
	{
		syslog(LOG_ERR, "[%lu]API_write: Socket nulo, Abortando função",(long unsigned int)pthread_self()%100);
		return -1;
	}
	if(msg == NULL)
	{
		syslog(LOG_ERR, "[%lu]API_write: Mensagem nula, Abortando função",(long unsigned int)pthread_self()%100);
		return -1;
	}
	if(msg_size < 0)
	{
		syslog(LOG_ERR, "[%lu]API_write: Tamanho da mensagem negativo, Abortando função",(long unsigned int)pthread_self()%100);
		return -1;		
	}
	char * aux = (char*)socket->socket_data_output.buffer;
	memcpy (&(aux[socket->socket_data_output.free_index]), msg, msg_size ); 

	socket->socket_data_output.free_index += msg_size;
	socket->select_ready= true;

	syslog(LOG_DEBUG, "[%lu]API_write: Buffer de output atualizado",(long unsigned int)pthread_self()%100);

	syslog(LOG_INFO, "API: Mensagem colocada na fila de escrita com sucesso\n");

	return 0;
}


int API_close_socket(info_socket_t* socket)
{	
	syslog(LOG_INFO, "API: Fechando Socket %d\n", socket->socket);

	syslog(LOG_DEBUG, "[%lu]API_close_socket()",(long unsigned int)pthread_self()%100);

	if(socket == NULL)
	{
		syslog(LOG_ERR, "[%lu]API_close_socket: Socket nulo, abortando função",(long unsigned int)pthread_self()%100);
		return -1;
	}

	socket->shutdown = true;

	syslog(LOG_DEBUG, "[%lu]API_close_socket: O socket %d foi desligado",(long unsigned int)pthread_self()%100, socket->socket);

	syslog(LOG_INFO, "API: O socket será removido da lista no proximo ciclo\n");
}



int API_shutdown()
{
	pthread_mutex_lock(IO_isocket_vec_mutex );
	
	for (int i = 0; i < MAX_IO_SOCKETS; ++i)
	{
		IO_isocket_vec[i].shutdown = true;
	}
	
	pthread_mutex_unlock(IO_isocket_vec_mutex );
	
	pthread_mutex_lock(SV_isocket_vec_mutex );
	
	for (int i = 0; i <MAX_SV_SOCKETS; ++i)
	{
		SV_isocket_vec[i].shutdown = true;
	}
	
	pthread_mutex_unlock(SV_isocket_vec_mutex );

	

	bool still_not_empty = 1;

	while (still_not_empty)
	{
		still_not_empty = 0;
		
		for (int i = 0; i < MAX_IO_SOCKETS; ++i)
		{
			if(IO_isocket_vec[i].socket_data_output.free_index > 0)
			{
				still_not_empty = 1;
			}
		}
	}

	stop_all();




	// implementar free's

}