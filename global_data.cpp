#include <pthread.h>
#include <semaphore.h>
#include "data_structs.h"
#include <string>
#include <stdio.h>
#include "API.h"
#include <syslog.h>

using namespace std;


int main(int argc, char * argv[])
{

	openlog("casca-log", LOG_PERROR, LOG_USER);
	//setlogmask (LOG_MASK(LOG_ERR)|LOG_MASK(LOG_WARNING)|LOG_MASK(LOG_INFO));
	API_Init(argc, argv);
	


	/*---------------------------------------------VArea de testesV------------------------------------------------------*/
	API_create_server(8181, NULL, NULL);	
	API_create_server(8383, NULL, NULL);
	
	info_socket_t* iS = API_create_socket("127.0.0.1",7272,NULL,NULL,NULL);	
	
	while(!iS->isConnected){}
	string txt = "123";
	API_write(iS,static_cast<void*>(&txt),3);
	txt = "456";
	API_write(iS,static_cast<void*>(&txt),3);
	txt = "789";
	API_write(iS,static_cast<void*>(&txt),3);
	txt = "abc";
	API_write(iS,static_cast<void*>(&txt),3);

	API_close_socket(iS);
	
	/*--------------------------------------------^Area de testes^-----------------------------------------------------*/



	sleep(100)	;
	closelog();	
	printf("FIM main\n");

	return 0;
}