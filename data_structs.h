#ifndef __DATA_STRUCTS__
#define __DATA_STRUCTS__

#include <stdbool.h>
#include "define.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>


typedef enum socket_type_enum {SERVER,IO} socket_type_t;
typedef int socket_t;


/* callbacks para processamento dos sockets de leitura e escrita*/
typedef long (* process_func_t)(void * buffer, long buffer_used);
typedef void (* send_done_func_t)();
typedef void  (*task_func)(void* current);

typedef struct {
	/* buffer de dados, cujo tamanho máximo é buffer_size.
	   buffer_used indica quantos bytes foram lidos até agora */
	void * buffer;
	long   buffer_size;
	long   buffer_used;

	/* callback chamada cada vez que novos dados são lidos.
	   Caso retorne valor não-nulo, significa que a mensagem completa chegou
	   e o valor retornado é o tamanho da mensagem completa. O buffer deve
	   ser "zerado", isto é, eliminar tudo entre [0, <retorno da funcao>
	   e buffer_used deve ser ajustado. */
	process_func_t  process;
} input_socket_data;


typedef struct {
	void * buffer;
	long   buffer_size;
	//long   buffer_sent;
	long free_index;
	/* callback quando buffer_sent == buffer_size !=0. Após sua execução, 
	   conteúdo deve ser zerado, com buffer_sent = buffer_size = 0*/
	send_done_func_t  send_done;
} output_socket_data;



typedef struct {
	/* identificador do socket */
	socket_t        socket;
	bool            select_ready;
	bool 			isConnected;
	/* tipo do socket:
	   Seu valor influenciará interpretação/cast de socket_data */
	socket_type_t   type;
	bool inRead;
	bool inWrite;
	struct sockaddr_in info;
	int id;
	bool shutdown;

	/*Sockets de transmição de dados*/
	input_socket_data socket_data_input;
	output_socket_data socket_data_output;
} info_socket_t;


typedef enum cmd_enum {SOCKET_READ, SOCKET_WRITE} cmd_t;

typedef struct {
	task_func cmd;
	info_socket_t * socket;
	task_func connected;
	//int    size_params;
	//void * params[MAX_CMD_PARAMS];
	void *  data_slot;
} info_cmd_t;

/* pode ser substituida por um deque<info_cmd_t> */
typedef struct info_cmd_queue {
	struct info_cmd_queue * prev;
	struct info_cmd_queue * next;	
	info_cmd_t       cmd;
	int id;
} info_cmd_queue_t;

/* struct de configuracao do do software principal*/
typedef struct config_struct
{
	int max_threads;

} config_struct_t;


typedef struct {

	info_socket_t* IOvec;
	
	pthread_mutex_t* IOvec_mutex;

} accept_socket_data;

//





#endif
