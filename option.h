#ifndef __OPTIONS__
#define __OPTIONS__

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <errno.h>	
#include <limits.h>
#include "define.h"
#include "data_structs.h"

/*
Função que imprime para o usuário como utilizar o programa na sua forma mais básica
*/
void
print_usage();

/*
Função que analiza as opções/flags passadas pelo usuário na execução do programa
*/
void
use_getopt(int * argc, char *** argv, config_struct_t * config);

#endif