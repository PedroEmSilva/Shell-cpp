#include "socket.h"
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include "data_structs.h"
#include "threads.h"
#include "option.h"
#include <stdio.h>
#include "commands.h"
#include <syslog.h>


info_socket_t* API_create_server(int porta, send_done_func_t *send_done_func, process_func_t *process_func);


void API_Init(int argc, char * argv[]);	


info_socket_t* API_create_socket(char*  address,int porta,send_done_func_t* send_done_func, 
								process_func_t* process_func,task_func* connected);


int API_write(info_socket_t* socket,void* buffer,long int buffersize); 

int API_close_socket(info_socket_t* socket);

int API_shutdown();