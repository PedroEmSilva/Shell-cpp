#include "socket.h"

void
check_socket(info_socket_t * socket_info, socket_type_t type)
{
	//Verifica se o socket esta pronto para ser trabalhado
	if(!socket_info->select_ready)
	{
		#ifdef DEBUGP
			printf("Socket is not ready.\n");
		#endif
		return;
	}

	//Caso seja um socket de input/output
	if(socket_info->type == IO)
	{
		if(type == IO)
		{
			
		}
		//Verifica se ha alguma entrada no socket_info->socket
		//Se houver recebe e carrega em ( (input_socket_data *) socket_info->socket_data) -> buffer
		//Adapta as outras variaveis da struct

		//Chamar outra thread para dar read ou write
		//REad!!!!
	}
	//Caso seja um socket de listen
	else 
	{
		//Acept para criar novos sockets
	}

}

int
socket_init(info_socket_t * socket_info, int port)
{
	syslog(LOG_DEBUG, "[%lu]socket_init()",pthread_self()%100);
	if(socket_info->type == IO)
	{
	
		socket_info->socket = socket(AF_INET,SOCK_STREAM, 0);

		if(socket_info->socket < 0)
		{
			
			perror("Socket creation error:\n");
			syslog(LOG_ERR, "[%lu]socket_init: Falha na inicialização do socket, para mais detalhes consulte stderr",pthread_self()%100);
			return -1;
		}
	}
	else
	{
		struct sockaddr_in server;
		server.sin_family = AF_INET;
		server.sin_addr.s_addr = INADDR_ANY;
		server.sin_port = htons(port);
		socket_info->socket = socket(AF_INET,SOCK_STREAM, 0);
		
		if(socket_info->socket < 0)
		{
			
			perror("Socket creation error:\n");
			syslog(LOG_ERR, "[%lu]socket_init: Falha na inicialização do socket, para mais detalhes consulte stderr",pthread_self()%100);
			return-1;
		}

		if(bind(socket_info->socket, (struct sockaddr *)&server, sizeof(server) ))
		{
			
			perror("Socket creation error:\n");
			syslog(LOG_ERR, "[%lu]socket_init: Falha na inicialização do socket, para mais detalhes consulte stderr",pthread_self()%100);
			return-1;
			
		}
		listen(socket_info->socket,1);
		socket_info->info = server;
	}

	socket_info->select_ready = true;
	socket_info->socket_data_output.buffer = NULL;
	syslog(LOG_DEBUG, "[%lu]socket_init: Socket inicializado com sucesso",pthread_self()%100);
	return 0 ;

}